package com.enonic.cms.plugin;

import com.enonic.cms.api.plugin.PluginEnvironment;
import com.enonic.cms.api.plugin.ext.auth.AuthenticationResult;
import com.enonic.cms.api.plugin.ext.auth.AuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class AuthenticatorPluginFirst extends com.enonic.cms.api.plugin.ext.auth.Authenticator {
    Logger LOG = LoggerFactory.getLogger(AuthenticatorPluginFirst.class);

    @Autowired
    PluginEnvironment pluginEnvironment;


    public AuthenticatorPluginFirst() {
        setDisplayName("AuthenticatorPluginFirst");
        setPriority(1);
    }

    @Override
    public AuthenticationResult authenticate(AuthenticationToken token) {
        LOG.info("\nAuthenticatorPlugin FirstInChain");
        LOG.info("requestURI: " + pluginEnvironment.getCurrentRequest().getRequestURI());
        LOG.info("Username: " + token.getUserName());
        LOG.info("Password: " + token.getPassword());
        LOG.info("Userstore: " + token.getUserStore());

        return AuthenticationResult.CONTINUE;
    }
}
