package com.enonic.cms.plugin;

import com.enonic.cms.api.plugin.PluginEnvironment;
import com.enonic.cms.api.plugin.ext.auth.AuthenticationResult;
import com.enonic.cms.api.plugin.ext.auth.AuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class AuthenticatorPluginSecond extends com.enonic.cms.api.plugin.ext.auth.Authenticator {
    Logger LOG = LoggerFactory.getLogger(AuthenticatorPluginSecond.class);

    @Autowired
    PluginEnvironment pluginEnvironment;

    public AuthenticatorPluginSecond() {
        setDisplayName("AuthenticatorPluginSecond");
        setPriority(2);
    }

    @Override
    public AuthenticationResult authenticate(AuthenticationToken token) {

        LOG.info("\nAuthenticatorPlugin SecondInChain");
        LOG.info("requestURI: " + pluginEnvironment.getCurrentRequest().getRequestURI());
        LOG.info("Username: " + token.getUserName());
        LOG.info("Password: " + token.getPassword());
        LOG.info("Userstore: " + token.getUserStore());

        return AuthenticationResult.CONTINUE;
    }
}
