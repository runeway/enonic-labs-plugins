package com.enonic.cms.plugin;

import com.enonic.cms.api.client.Client;
import com.enonic.cms.api.client.model.GetGroupsParams;
import com.enonic.cms.api.client.model.GetUserParams;
import com.enonic.cms.api.client.model.GetUsersParams;
import com.enonic.cms.api.plugin.ext.http.HttpAutoLogin;
import org.jdom.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by rfo on 04/06/14.
 */
@Component
public class AutoLoginExt extends HttpAutoLogin {

    Logger LOG = LoggerFactory.getLogger(AutoLoginExt.class);

    @Autowired
    Client client;

    public AutoLoginExt(){
        setDisplayName("Autologin ext");
        setPriority(0);
        setUrlPatterns(new String[]{"/site/[0-99]/.*","/site/0/.*", "/site/16/.*"});
    }

    @Override
    public String getAuthenticatedUser(HttpServletRequest request) throws Exception {
        LOG.info("AutoLoginExt: login rfo");
        System.out.println("-----------------AutoLoginExt-------getUser------------");

        GetUserParams getUserParams = new GetUserParams();
        getUserParams.user="4:rfo";
        getUserParams.includeCustomUserFields=true;
        getUserParams.includeMemberships=true;
        getUserParams.normalizeGroups=true;
        client.impersonate("4:amAdmin");
        Document userDoc = client.getUser(getUserParams);
        client.removeImpersonation();
        Helper.prettyPrint(userDoc);

        return "#4:rfo";
    }
}
